from enum import Enum, auto

from Plante import Plante
from Ferme import Ferme

class Saison(Enum):
    PRINTEMPS = auto()
    ETE = auto()
    AUTOMNE = auto()
    HIVER = auto()